#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 16:00:50 2019

@author: Ferdi, Sarah, Miro
"""

import pyvisa 
import time
import matplotlib.pyplot as plt
import numpy as np

rm = pyvisa.ResourceManager()
rm.list_resources()
my_instrument = rm.open_resource('USB0::0x0957::0x2007::MY59001715::INSTR')

#Switch_close is a subfunktion for channel_seclect.
def switch_close(space):
    tmp='ROUTe:CLOS (@' +space+')'
    print(tmp)
    my_instrument.write(tmp)

#Switch_open is the reset funktion for wrongly closed channels.    
def switch_open():
     space =input()
     tmp='ROUTe:OPEN (@' + (str(space))+')'
     print(tmp)
     my_instrument.write(tmp)

#Switch_reset is the reset funktion for all channels.    
def switch_reset():
    tmp= 'ROUTe:OPEN (@111:148)'
    print(tmp)
    my_instrument.write(tmp)
    
#Switch_read shows you if the channel is open or closed.
def switch_read():
    space= input()
    tmp='ROUTe:READ? (@' + (str(space))+')'
    print(tmp)
    my_instrument.write(tmp)
    info = my_instrument.read(tmp)
    print(info)
    
#Error_check shows acuring errors
def error_check():
    info= my_instrument.query ('SYSTem:ERRor?')
    print (info)

#channel_select closes a specific channel
def channel_select():
    switch_reset()
    time.sleep(2)
    karte=str(input("Welche Karte wird verwendet? 1-3 \n"))
    eingang=str(input("Welcher Eingang soll geschaltet werden? 1-4 \n"))
    ausgang=str(input("Wo soll das Signal hingehen? 1-8 \n"))
    space=''
    lists=''
    for x in ausgang:
        space=karte+eingang+x
        if x==',':
            lists=lists+','
        else:
            lists=lists+space
    switch_close(lists)
   
#Display displays a text (up to 12 characters) on the front display.
def display():
    text=input()
    my_instrument.write("DISP:TEXT '"+text+"'")
    time.sleep(5)
    my_instrument.write("DISP:TEXT:CLEar")

#cycle_count is a sub funktion for the plot_sycount.
def cycle_count():
    space= input()
    tmp='DIAG:REL:CYCL? (@' + (str(space))+')'
    print(tmp)
    count=my_instrument.query(tmp)
    print(count)
    return count

#plot_sycount prints a Histrogram of the Cyclecounts.
def plot_cycount():
    count_string=cycle_count()
    clist=count_string.split(',')
    xlist=[]
    for x in range(len(clist)):
        clist[x]=int(clist[x])
        xlist.append(x+1)
    plt.plot(xlist,clist)
    plt.xlabel('Channel')
    plt.ylabel('Amount of switches')
    plt.title('Diagram of Cyclecounts')
    plt.show()


plot_cycount()     
#cycle_count()
#disp_off()
#error_check()
#display()
#switch_reset()  
#channel_select()
#switch_open()

# Important to close the connection again
my_instrument.close()    