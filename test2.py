#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 16:00:50 2019

@author: Ferdi, Sarah, Miro
"""

import pyvisa 
import time
import matplotlib.pyplot as plt
import numpy as np

rm = pyvisa.ResourceManager()
rm.list_resources()
my_instrument = rm.open_resource('USB0::0x0957::0x2007::MY59001715::INSTR')

def switch_close(space):
    tmp='ROUTe:CLOS (@' +space+')'
    print(tmp)
    my_instrument.write(tmp)
    
def switch_open(space):
     tmp='ROUTe:OPEN (@' +space+')'
     print(tmp)
     my_instrument.write(tmp)
     
def switch_reset():
    tmp= 'ROUTe:OPEN (@111:148)'
    print(tmp)
    my_instrument.write(tmp)
    
def switch_read():
    space= input()
    tmp='ROUTe:READ? (@' + (str(space))+')'
    print(tmp)
    my_instrument.write(tmp)
    info = my_instrument.read(tmp)
    print(info)
    
def error_check():
    info= my_instrument.query ('SYSTem:ERRor?')
    print (info)
    
#def disp_off():
    #my_instrument.write('DISPlay{OFF}')

def channel_select():
    karte=str(input("Which Card do you want to adress? 1-3 \n"))
    eingang=str(input("Which Input do you want to use? 1-4 \n"))
    ausgang=str(input("Where Output do you want to use? 1-8 \n"))
    space=''
    lists=''
    eingang=eingang.split(',')
    ausgang=ausgang.split(',')
    z=0
    for t in eingang:
        for x in ausgang:
            if t==eingang[-1]and x==ausgang[-1]:
                z=1
            space=karte+t+x
            if z==0:
                lists=lists+space+','
            else:
                lists=lists+space
    return lists
   
def display():
    text=input("What text do you want to show?\n")
    my_instrument.write("DISP:TEXT '"+text+"'")
    time.sleep(5)
    my_instrument.write("DISP:TEXT:CLEar")



def cycle_count(space):
    tmp='DIAG:REL:CYCL? (@' +space+')'
    print(tmp)
    count=my_instrument.query(tmp)
    print(count)
    return count

def plot_cycount():
    count_string=cycle_count(channel_select())
    clist=count_string.split(',')
    xlist= []
    for x in range(len(clist)):
        clist[x]=int(clist[x])
    for x in range(len(clist)):
        xlist.append(x)
    plt.hist(clist,xlist)
    plt.xlabel('Channel')
    plt.ylabel('Amount of switches')
    plt.title('Diagram of Cyclecounts')
    plt.show()

        
def voltage_dc():
    space= input()
    print (my_instrument.query('MEASure:VOLTage:DC?(@' + (str(space))+')'))
    time.sleep(2)
    my_instrument.write()
    
def meas_temp():
    my_instrument.write("MEASure:TEMPerature?(@111)")
    print(my_instrument.read("MEASure:TEMPerature?(@111)"))

goto=0    
while(goto==0):
    menu=int(input("What do you want to do?\n press 0 for reset of card \n press 1 for sending a signal to an output \n" 
               " press 2 for opening a several switch\n press 3 to read out the error storage\n "
               "press 4 to show a certain text on the display \n press 5 to show a plot of the amount of times each switch was used \n"
               " press 6 if you want to quit the programm. \n"))
    if(menu==0):
        switch_reset()
    elif(menu==1):
        switch_close(channel_select())
    elif(menu==2):
        switch_open(channel_select())
    elif(menu==3):
        error_check()
    elif(menu==4):
        display()
    elif(menu==5):
        plot_cycount()
    elif(menu==6):
        goto=1


# Important to close the connection again
my_instrument.close()    

    
    