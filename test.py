#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 16:00:50 2019

@author: Ferdi, Sarah, Miro
"""

import pyvisa 
import time

rm = pyvisa.ResourceManager()
rm.list_resources()
my_instrument = rm.open_resource('USB0::0x0957::0x2007::MY59001715::INSTR')


def switch_close():
    space =input()

    tmp='ROUTe:CLOS (@' + (str(space))+')'
    print(tmp)
    my_instrument.write(tmp)
    
def switch_open():

     space =input()
     tmp='ROUTe:OPEN (@' + (str(space))+')'
     print(tmp)
     my_instrument.write(tmp)
     
def switch_reset():
    tmp= 'ROUTe:OPEN (@111:148)'
    print(tmp)
    my_instrument.write(tmp)
    
def switch_read():
    space= input()
    tmp='ROUTe:READ? (@' + (str(space))+')'
    print(tmp)
    my_instrument.write(tmp)
    info = my_instrument.read(tmp)
    print(info)
         
def error_check():
    info= my_instrument.query ('SYSTem:ERRor?')
    print (info)
    
#def disp_off():
    #my_instrument.write('DISPlay{OFF}')

def channel_select():
    return ('')
   
def display():
    text=input()
    my_instrument.write("DISP:TEXT '"+text+"'")
    time.sleep(5)
    my_instrument.write("DISP:TEXT:CLEar")

def cycle_count():
    space= input()
    tmp='DIAG:REL:CYCL? (@' + (str(space))+')'
    print(tmp)
    print(my_instrument.query(tmp))
    #info = my_instrument.read(tmp)
    #print(info)
        
def voltage_dc():
    space= input()
    print (my_instrument.query('MEASure:CURRent:DC?(@' + (str(space))+')'))
    
def meas_temp():
    my_instrument.write("MEASure:TEMPerature?(@111)")
    print(my_instrument.read("MEASure:TEMPerature?(@111)"))
    
     
#voltage_dc()
#cycle_count()
#switch_close()
#disp_off()
#time.sleep(2)
#error_check()
#display()
switch_reset()   
time.sleep(2)
meas_temp()

#switch_open()
#time.sleep(2)

#time.sleep(2)
# Important to close the connection again
my_instrument.close()
